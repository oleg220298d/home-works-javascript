/**
 * Клас, об'єкти якого описують параметри гамбургера.
 *
 *
 * @constructor
 * @param size Розмір
 * @param stuffing Начинка
 * @throws {HamburgerException} При неправильному використанні
 */
function Hamburger(size, stuffing) {
    if (arguments.length < 2) {
        throw new HamburgerException('Потрібні два аргументи: ' + arguments.length);
    }
    if (Hamburger.allowedSizes.indexOf(size) < 0) {
        throw new HamburgerException('Недійсний розмір!');
    }
    if (Hamburger.allowedStuffing.indexOf(stuffing) < 0) {
        throw new HamburgerException('Недійсна начинка!');
    }

    this.size = size;
    this.stuffing = stuffing;
}

/* Розміри, види начинок та добавок */
Hamburger.SIZE_SMALL = { name: 'small', price: 50, calories: 20, };
Hamburger.SIZE_LARGE = { name: 'large', price: 100, calories: 40, };
Hamburger.STUFFING_CHEESE = { name: 'cheese', price: 10, calories: 20, };
Hamburger.STUFFING_SALAD = { name: 'salad', price: 20, calories: 5, };
Hamburger.STUFFING_POTATO = { name: 'potato', price: 15, calories: 10, };
Hamburger.TOPPING_MAYO = { name: 'mayo', price: 20, calories: 5, };
Hamburger.TOPPING_SPICE = { name: 'spice', price: 15, calories: 0, };

Hamburger.allowedToppings = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE];
Hamburger.allowedSizes = [Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE];
Hamburger.allowedStuffing = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_POTATO, Hamburger.STUFFING_SALAD]

/**
 * Додати добавку до гамбургера. Можна додати кілька
 * Добавок, за умови, що вони різні.
 *
 * @param topping Тип добавки
 * @throws {HamburgerException} При неправильному використанні
 */
Hamburger.prototype.addTopping = function (topping) {
    if (arguments.length !== 1) {
        throw new HamburgerException('Потрібен один аргумент: ' + arguments.length)
    }
    if (Hamburger.allowedToppings.indexOf(topping) < 0) {
        throw new HamburgerException('Невірна начинка!')
    }
    if (!('toppings' in this)) {
        this.toppings = [];
    } else if (this.toppings.indexOf(topping) >= 0) {
        throw new HamburgerException('Копія начинки!')
    }

    this.toppings.push(topping)
}

/**
 * Прибрати добавку, за умови, що вона раніше була
 * Додано.
 *
 * @param topping Тип добавки
 * @throws {HamburgerException} При неправильному використанні
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (arguments.length !== 1) {
        throw new HamburgerException('Потрібен один аргумент: ')
    }
    if (Hamburger.allowedToppings.indexOf(topping) < 0) {
        throw new HamburgerException('Невірна начинка!')
    }
    if (this.toppings.indexOf(topping) < 0) {
        throw new HamburgerException('Гамбурге не має начинки!')
    }

    delete this.toppings[topping];
}

/**
 * Отримати список добавок.
 *
 * @return {Array} Масив доданих добавок містить константи
 * Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.toppings
}

/**
 * Дізнатися розмір гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size
}

/**
 * Дізнатися начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing
}

/**
 * Дізнатись ціну гамбургера
 * @return {Number} Ціна у тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    const size = this.getSize();
    let price = size['price'];

    const toppings = this.getToppings();
    toppings.forEach(function (item) {
        price += item['price']
    });

    const stuffing = this.getStuffing();
    price += stuffing['price'];

    return price
}

/**
 * Дізнатися калорійність
 * @return {Number} Калорійність калорій
 */
Hamburger.prototype.calculateCalories = function () {
    const size = this.getSize();
    let calories = size['calories'];

    const toppings = this.getToppings();
    toppings.forEach(function (item) {
        calories += item['calories']
    });

    const stuffing = this.getStuffing();
    calories += stuffing['calories']

    return calories
}

/**
 * Надає інформацію про помилку під час роботи з гамбургером.
 * Подробиці зберігаються у властивості message.
 * @constructor
 */
function HamburgerException(message) {
    this.message = message
}

// маленький гамбургер із начинкою із сиру
const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// Добавка з майонезу
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// Запитаємо скільки там калорій
console.log("Calories: %f", hamburger.calculateCalories());
// скільки коштує
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумав і вирішив додати ще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А скільки тепер коштує?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Перевірити, чи великий гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Забрати добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

// не передали обов'язкові параметри
const h2 = new Hamburger(); // => HamburgerException: no size given

// передаємо некоректні значення, добавку замість розміру
const h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// додаємо багато добавок
const h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'