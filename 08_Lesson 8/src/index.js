window.onload = () => {
    const textarea = document.querySelector("textarea"),
        button = document.querySelector("button"),
        div = document.createElement("div");

    button.onclick = () => {
        const text = textarea.value;
        div.innerText = text;
        button.after(div)
        textarea.value = ""
    }

    data.exchangeRate.forEach(element => {
        console.log(element)
    });
}

const data = {"date":"24.10.2021","bank":"PB","baseCurrency":980,"baseCurrencyLit":"UAH","exchangeRate":[{"baseCurrency":"UAH","saleRateNB":19.7136000,"purchaseRateNB":19.7136000},{"baseCurrency":"UAH","currency":"AZN","saleRateNB":15.6620000,"purchaseRateNB":15.6620000},{"baseCurrency":"UAH","currency":"BYN","saleRateNB":10.8869000,"purchaseRateNB":10.8869000},{"baseCurrency":"UAH","currency":"CAD","saleRateNB":21.3330000,"purchaseRateNB":21.3330000},{"baseCurrency":"UAH","currency":"CHF","saleRateNB":28.6762000,"purchaseRateNB":28.6762000,"saleRate":29.1000000,"purchaseRate":27.5000000},{"baseCurrency":"UAH","currency":"CNY","saleRateNB":4.1141000,"purchaseRateNB":4.1141000},{"baseCurrency":"UAH","currency":"CZK","saleRateNB":1.1919000,"purchaseRateNB":1.1919000,"saleRate":1.3000000,"purchaseRate":1.1000000},{"baseCurrency":"UAH","currency":"DKK","saleRateNB":4.1096000,"purchaseRateNB":4.1096000},{"baseCurrency":"UAH","currency":"EUR","saleRateNB":30.5774000,"purchaseRateNB":30.5774000,"saleRate":30.8000000,"purchaseRate":30.2000000},{"baseCurrency":"UAH","currency":"GBP","saleRateNB":36.2511000,"purchaseRateNB":36.2511000,"saleRate":37.2000000,"purchaseRate":35.2000000},{"baseCurrency":"UAH","currency":"HUF","saleRateNB":0.0841230,"purchaseRateNB":0.0841230},{"baseCurrency":"UAH","currency":"ILS","saleRateNB":8.1970000,"purchaseRateNB":8.1970000},{"baseCurrency":"UAH","currency":"JPY","saleRateNB":0.2310000,"purchaseRateNB":0.2310000},{"baseCurrency":"UAH","currency":"KZT","saleRateNB":0.0618140,"purchaseRateNB":0.0618140},{"baseCurrency":"UAH","currency":"MDL","saleRateNB":1.5065000,"purchaseRateNB":1.5065000},{"baseCurrency":"UAH","currency":"NOK","saleRateNB":3.1485000,"purchaseRateNB":3.1485000},{"baseCurrency":"UAH","currency":"PLN","saleRateNB":6.6536000,"purchaseRateNB":6.6536000,"saleRate":6.8000000,"purchaseRate":6.5000000},{"baseCurrency":"UAH","currency":"RUB","saleRateNB":0.3738500,"purchaseRateNB":0.3738500,"saleRate":0.3870000,"purchaseRate":0.3570000},{"baseCurrency":"UAH","currency":"SEK","saleRateNB":3.0647000,"purchaseRateNB":3.0647000},{"baseCurrency":"UAH","currency":"SGD","saleRateNB":19.5173000,"purchaseRateNB":19.5173000},{"baseCurrency":"UAH","currency":"TMT","saleRateNB":7.6050000,"purchaseRateNB":7.6050000},{"baseCurrency":"UAH","currency":"TRY","saleRateNB":2.7337000,"purchaseRateNB":2.7337000},{"baseCurrency":"UAH","currency":"UAH","saleRateNB":1.0000000,"purchaseRateNB":1.0000000},{"baseCurrency":"UAH","currency":"USD","saleRateNB":26.2918000,"purchaseRateNB":26.2918000,"saleRate":26.5000000,"purchaseRate":26.1000000},{"baseCurrency":"UAH","currency":"UZS","saleRateNB":0.0024895,"purchaseRateNB":0.0024895},{"baseCurrency":"UAH","currency":"GEL","saleRateNB":8.5403000,"purchaseRateNB":8.5403000}]}